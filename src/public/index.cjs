fetch("./output/1-matches-per-year.json")
  .then((data) => data.json())
  .then((data) => {
    const matchesPlayed = Object.entries(data);
    Highcharts.chart('1-matches-per-year', {
      chart: {
        type: 'line',
        options3d: {
          enabled: true,
          alpha: 5,
          beta: 10,
          depth: 50,
          viewDistance: 60,
        }
      },

      title: {
        text: 'Matches Played Per Year, 2008-2017',
        align: 'left'
      },

      yAxis: {
        title: {
          margin: 50,
          text: 'matches'
        }
      },

      xAxis: {
        accessibility: {
          rangeDescription: 'Range: 2008 to 2017'
        },
        title: {
          margin: 50,
          text: 'Seasons'
        }
      },

      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },

      plotOptions: {
        series: {
          label: {
            connectorAllowed: false
          },
          depth: 50,
          colorByPoint: true,
          pointStart: 2008
        }
      },

      series: [{
        name: '',
        data: matchesPlayed,
        showInLegend: false
      }],

      responsive: {
        rules: [{
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }

    });
  });

fetch("./output/2-matches-won-per-team-per-year.json")
  .then((data) => data.json())
  .then((data) => {

    let years = Object.assign(...(Object.values(data)));
    years = Object.keys(years);
    let seriesData = [];

    let matchesWonPerTeamPerYear = Object.entries(data);
    matchesWonPerTeamPerYear.map(([team, seasons]) => {
      let teamsWinningStreak = {};
      teamsWinningStreak['name'] = team;
      years.map((year) => {
        if (!(year in seasons)) {
          seasons[year] = "not played";
        }
      })
      teamsWinningStreak['data'] = Object.values(seasons);
      if (teamsWinningStreak['name'] !== '')
        seriesData.push(teamsWinningStreak);
    });
    // console.log(seriesData.slice(0,5))
    Highcharts.chart('2-matches-won-per-team-per-year', {
      chart: {
        type: 'line'
      },

      title: {
        text: 'Matches won per team per year 2016',
        align: 'left'
      },

      yAxis: {
        title: {
          margin: 50,
          text: 'matches won',
        }
      },

      xAxis: {
        accessibility: {
          rangeDescription: 'Range: 2008 to 2017'
        },
        title: {
          margin: 50,
          text: 'Seasons'
        }
      },

      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },

      plotOptions: {
        series: {
          label: {
            connectorAllowed: false
          },
          depth: 50,
          pointStart: 2008,
        }
      },

      series: seriesData,

      responsive: {
        rules: [{
          condition: {
            maxWidth: 400
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }

    });
  });

fetch("./output/3-extra-runs-conceded-per-team-in-2016.json")
  .then((data) => data.json())
  .then((data) => {
    const extraRunPerTeam = Object.entries(data);
    Highcharts.chart('3-extra-runs-conceded-per-team-in-2016', {
      chart: {
        type: 'column',
        options3d: {
          enabled: true,
          alpha: 5,
          beta: 10,
          depth: 50,
          viewDistance: 60,
        }
      },

      title: {
        text: 'Extra runs conceded per team in 2016',
        align: 'left'
      },

      yAxis: {
        title: {
          margin: 50,
          text: 'winning'
        }
      },

      xAxis: {
        type: 'category',
        title: {
          margin: 50,
          text: 'Teams'
        }
      },

      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },

      plotOptions: {
        series: {
          dataLabels: {
            enabled: true,
            format: '{point.y:.f}'
          },
          label: {
            connectorAllowed: true
          },
          depth: 50,
          colorByPoint: true,
          pointStart: 2008
        }
      },

      series: [{
        name: '',
        data: extraRunPerTeam,
        showInLegend: false
      }],

      responsive: {
        rules: [{
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }

    });
  });

fetch("./output/4-top-10-economical-bowlers-in-2015.json")
  .then((data) => data.json())
  .then((data) => {
    let valuesOfPosition = Object.values(data);
    let bowlerAndEconomy = [];
    valuesOfPosition.map((postion) => {
      let bowler = [];
      bowler.push(postion.Bowler);
      bowler.push(Number(postion.Economy));
      bowlerAndEconomy.push(bowler);
    });
    Highcharts.chart('4-top-10-economical-bowlers-in-2015', {
      chart: {
        type: 'bar',
        options3d: {
          enabled: true,
          alpha: 5,
          beta: 10,
          depth: 50,
          viewDistance: 60,
        }
      },

      title: {
        text: 'top 10 economical bowlers in 2015',
        align: 'left'
      },

      yAxis: {
        title: {
          margin: 50,
          text: 'Run Rate'
        }
      },

      xAxis: {
        type: 'category',
        title: {
          margin: 50,
          text: 'Bowlers'
        }
      },

      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },

      plotOptions: {
        series: {
          dataLabels: {
            enabled: true,
            format: '{point.y:.f}'
          },
          label: {
            connectorAllowed: true
          },
          depth: 50,
          colorByPoint: true,
          pointStart: 2008
        }
      },

      series: [{
        name: '',
        data: bowlerAndEconomy,
        showInLegend: false
      }],

      responsive: {
        rules: [{
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }

    });
  });
// 5-team-won-toss-and-match.json
fetch("./output/5-team-won-toss-and-match.json")
  .then((data) => data.json())
  .then((data) => {
    const matchesPlayed = Object.entries(data);
    Highcharts.chart('5-team-won-toss-and-match', {
      chart: {
        type: 'bar',
        options3d: {
          enabled: true,
          alpha: 5,
          beta: 10,
          depth: 50,
          viewDistance: 60,
        }
      },

      title: {
        text: 'Team won toss and match, 2008-2017',
        align: 'left'
      },

      yAxis: {
        title: {
          margin: 50,
          text: 'matches'
        }
      },

      xAxis: {
        type: 'category',
        title: {
          margin: 50,
          text: 'Teams'
        }
      },

      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },

      plotOptions: {
        series: {
          dataLabels: {
            enabled: true,
            format: '{point.y:.f}'
          },
          label: {
            connectorAllowed: true
          },
          depth: 50,
          colorByPoint: true,
          pointStart: 2008
        }
      },

      series: [{
        name: '',
        data: matchesPlayed,
        showInLegend: false
      }],

      responsive: {
        rules: [{
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }

    });
  });

fetch("./output/6-player-of-the-match-for-each-season.json")
  .then((data) => data.json())
  .then((data) => {
    const playerName = 0;
    const winningCount = 1;
    let seasons = Object.keys(data);
    let playerWinsCount = []
    seasons.map((season) => {
      let player = {};
      player[Object.values(data[season])[playerName]] = (Object.values(data[season])[winningCount]);
      playerWinsCount.push(Object.entries(player).flat());
    });
    console.log(playerWinsCount);
    Highcharts.chart('6-player-of-the-match-for-season', {
      chart: {
        type: 'column',
        options3d: {
          enabled: true,
          alpha: 5,
          beta: 10,
          depth: 50,
          viewDistance: 60,
        }
      },

      title: {
        text: 'Player of the match for each season',
        align: 'left'
      },

      yAxis: {
        title: {
          margin: 50,
          text: 'matches'
        }
      },

      xAxis: {
        categories: seasons,
        type: 'category',
        title: {
          margin: 50,
          text: 'Teams'
        }
      },

      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },

      plotOptions: {
        series: {
          dataLabels: {
            enabled: true,
            format: '{point.:.f}'
          },
          label: {
            connectorAllowed: true
          },
          depth: 50,
          colorByPoint: true,
          pointStart: 2008
        }
      },

      series: [{
        name: 'Won',
        data: playerWinsCount,
        showInLegend: true
      }],

      responsive: {
        rules: [{
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }

    });
  });

fetch("./output/7-strike-rate-of-batsman-for-each-season.json")
  .then((data) => data.json())
  .then((data) => {

    let years = Object.assign(...(Object.values(data)));
    years = Object.keys(years);
    let seriesData = [];

    let batsman = Object.entries(data);
    batsman.map(([team, seasons]) => {
      let batsmanAndStrikeRate = {};
      batsmanAndStrikeRate['name'] = team;
      years.map((year) => {
        if (!(year in seasons)) {
          seasons[year] = "not played";
        }
      })
      batsmanAndStrikeRate['data'] = Object.values(seasons);
      if (batsmanAndStrikeRate['name'] !== '')
        seriesData.push(batsmanAndStrikeRate);
    });
    Highcharts.chart('7-strike-rate-of-batsman-for-each-season', {
      chart: {
        type: 'column'
      },

      title: {
        text: 'strike rate of batsman for each season',
        align: 'left'
      },

      yAxis: {
        title: {
          margin: 50,
          text: 'matches won',
        }
      },

      xAxis: {
        accessibility: {
          rangeDescription: 'Range: 2008 to 2017'
        },
        title: {
          margin: 50,
          text: 'Seasons'
        }
      },

      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },

      plotOptions: {
        series: {
          label: {
            connectorAllowed: false
          },
          pointWidth: 15,
          depth: 50,
          pointStart: 2008,
        }
      },

      series: seriesData,

      responsive: {
        rules: [{
          condition: {
            maxWidth: 10,
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }

    });
  });

fetch("./output/8-highest-number-of-times-one-player-has-been-dismissed-by-another-player.json")
  .then((data) => data.json())
  .then((data) => {

    let seriesToPlot = Object.values(data);
    let xAxisValue = Object.keys(data);

    Highcharts.chart('8-highest-number-of-times-one-player-has-been-dismissed-by-another-player', {
      chart: {
        type: 'column'
      },

      title: {
        text: 'Highest number of times one player has been dismissed by another player',
        align: 'left'
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: xAxisValue,
        }
      },
      legend: {
        enabled: false
      },
      
      series: [{
        name: '',
        data: seriesToPlot,
        dataLabels: {
          enabled: true,
          color: '#FFFFFF',
          align: 'right',
          format: '{point.y:.1f}', // one decimal
          x: -50,
          y: 50, // 10 pixels down from the top
          style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif'
          }
      }
      }],

      responsive: {
        rules: [{
          condition: {
            maxWidth: 10,
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }

    });
  });

  fetch("./output/9-best-economy-bowler-superover.json")
  .then((data) => data.json())
  .then((data) => {

    let bowlerName =Object.keys(data).toString();
    let bowlerEconomy = Object.values(data);

    Highcharts.chart('9-best-economy-bowler-superover', {
      chart: {
        type: 'column'
      },

      title: {
        text: 'Best economy bowler superover',
        align: 'left'
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        
      },
      legend: {
        enabled: false
      },
      
      series: [{
        name: bowlerName,
        data: bowlerEconomy,
        dataLabels: {
          enabled: true,
          color: '#FFFFFF',
          align: 'right',
          format: '{point.y:.1f}', // one decimal
          x: -50,
          y: 50, // 10 pixels down from the top
          style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif'
          }
      }
      }],

      responsive: {
        rules: [{
          condition: {
            maxWidth: 10,
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }

    });
  });


  