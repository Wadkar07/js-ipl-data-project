
function strikeRateOfBatsmanForEachSeason(matches, deliveries) {

    if (matches === undefined || !Array.isArray(matches)) {
        return {};
    }
    else {
        const idAndSeason = matches.reduce((idAndSeason, match) => {
            idAndSeason[match.id] = match.season;
            return idAndSeason;
        }, {});

        const strikeRateOfBatsmanPerYear = deliveries.reduce((strikeRateOfBatsmanPerYear, delivery) => {
            if (strikeRateOfBatsmanPerYear[delivery.batsman] === undefined) {
                strikeRateOfBatsmanPerYear[delivery.batsman] = {};
            }

            if (strikeRateOfBatsmanPerYear[delivery.batsman][idAndSeason[delivery.match_id]] === undefined) {
                if (delivery.wide_runs === '0') {
                    strikeRateOfBatsmanPerYear[delivery.batsman][idAndSeason[delivery.match_id]] = {
                        runs: Number(delivery.batsman_runs),
                        balls: 1,
                        strikeRate: ((this.runs / this.balls) * 100).toFixed(2)
                    };
                }
                else {
                    strikeRateOfBatsmanPerYear[delivery.batsman][idAndSeason[delivery.match_id]] = {
                        runs: Number(delivery.batsman_runs),
                        balls: 0,
                        strikeRate: 0
                    };
                }
            }
            else {
                strikeRateOfBatsmanPerYear[delivery.batsman][idAndSeason[delivery.match_id]].runs += Number(delivery.batsman_runs);

                if (delivery.wide_runs === '0') {
                    strikeRateOfBatsmanPerYear[delivery.batsman][idAndSeason[delivery.match_id]].balls++;
                }

                strikeRateOfBatsmanPerYear[delivery.batsman][idAndSeason[delivery.match_id]].strikeRate = ((strikeRateOfBatsmanPerYear[delivery.batsman][idAndSeason[delivery.match_id]].runs / strikeRateOfBatsmanPerYear[delivery.batsman][idAndSeason[delivery.match_id]].balls) * 100).toFixed(2);
            }
            return strikeRateOfBatsmanPerYear;
        }, {});
        let batsman = Object.entries(strikeRateOfBatsmanPerYear);
        let batsmanAndStrikeRate = {};
        batsman.map((player) => {
            batsmanAndStrikeRate[player[0]] = {};
            let years = Object.keys(player[1]);
            years.map((year) => {
                batsmanAndStrikeRate[player[0]][year] = Number(player[1][year].strikeRate);
            })
        });
        return batsmanAndStrikeRate;
    }
}
module.exports = strikeRateOfBatsmanForEachSeason;