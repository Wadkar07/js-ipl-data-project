function top10EconomicalBowlerIn2015(matches, deliveries) {
    if (matches === undefined || !Array.isArray(matches)) {
        return {};
    }
    else {
        const idOf2015 = matches.reduce((matchIds, match) => {
            if (match.season == 2015) {
                return matchIds.concat(match.id);
            }
            return matchIds;
        }, []);
        const runsPerBowlers = deliveries.reduce((runsPerBowlers, delivery) => {
            if (idOf2015.includes(delivery.match_id)) {
                if (runsPerBowlers[delivery.bowler] !== undefined) {
                    runsPerBowlers[delivery.bowler]['runs'] += Number(delivery.total_runs) - Number(delivery.legbye_runs) - Number(delivery.bye_runs) - Number(delivery.penalty_runs);
                }
                else {
                    runsPerBowlers[delivery.bowler] = {};
                    runsPerBowlers[delivery.bowler]['runs'] = Number(delivery.total_runs) - Number(delivery.legbye_runs) - Number(delivery.bye_runs) - Number(delivery.penalty_runs);
                }
                return runsPerBowlers;
            }
            return runsPerBowlers;
        }, {});

        const ballsPerBowlers = deliveries.reduce((ballsPerBowlers, delivery) => {
            if (idOf2015.includes(delivery.match_id)) {
                if ((ballsPerBowlers[delivery.bowler] !== undefined)) {
                    if (delivery.wide_runs === '0' && delivery.noball_runs === '0') {
                        ballsPerBowlers[delivery.bowler]['balls']++;
                    }
                }
                else {
                    ballsPerBowlers[delivery.bowler] = {};
                    ballsPerBowlers[delivery.bowler]['balls'] = 1;
                }
                return ballsPerBowlers;
            }
            return ballsPerBowlers;
        }, {});

        const bowlersOf2015Matches = Object.keys(ballsPerBowlers);
        let bowlerAndEconomy = {}

        bowlersOf2015Matches.forEach((bowler) => {
            bowlerAndEconomy[bowler] = (Number(runsPerBowlers[bowler]['runs']) / (Number(ballsPerBowlers[bowler]['balls']) / 6)).toFixed(2);
        });

        bowlerAndEconomy = Object.fromEntries(
            Object
                .entries(bowlerAndEconomy)
                .sort(([, currentBowler], [, nextBowler]) => currentBowler - nextBowler)
                .slice(0, 10)
        );
        let index = 1;
        const top10EconomicBowler = {};
        for (let bowler in bowlerAndEconomy) {
            top10EconomicBowler[`Position : ${index}`] = {};
            top10EconomicBowler[`Position : ${index}`]['Bowler'] = bowler;
            top10EconomicBowler[`Position : ${index}`]['Economy'] = bowlerAndEconomy[bowler];
            index++;
        }
        return top10EconomicBowler;

    }
}
module.exports = top10EconomicalBowlerIn2015;