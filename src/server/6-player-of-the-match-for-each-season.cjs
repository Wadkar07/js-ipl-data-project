function playerOfTheMatchForEachSeason(matches) {
    if (matches === undefined || !Array.isArray(matches)) {
        return {};
    }
    else {
        let playersOfMatch = matches.map(match => match.player_of_match);
        let seasons = matches.map(match => match.season);
        playersOfMatch = [...new Set(playersOfMatch)];
        seasons = [...new Set(seasons)];

        let playerOfTheMatchPerSeason = {};

        seasons.forEach((season) => {
            playerOfTheMatchPerSeason[season] = {}
            let maximumPlayerOfMatchCount = 0;
            let playerName = '';

            playersOfMatch.forEach(playerOfMatch => {
                const playerOfMatchCount = matches.filter((match) => {
                    return match.player_of_match === playerOfMatch && match.season === season;
                })

                if (Object.keys(playerOfMatchCount).length > maximumPlayerOfMatchCount) {
                    maximumPlayerOfMatchCount = Object.keys(playerOfMatchCount).length;
                    playerName = playerOfMatch;
                }
            });
            playerOfTheMatchPerSeason[season]['Player'] = playerName;
            playerOfTheMatchPerSeason[season]['Player of Match count'] = maximumPlayerOfMatchCount;
        })
        return playerOfTheMatchPerSeason;
    }
}
module.exports = playerOfTheMatchForEachSeason;