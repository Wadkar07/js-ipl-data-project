function extraRunsConcededPerTeamIn2016(matches, deliveries) {
    if (matches === undefined || !Array.isArray(matches)) {
        return {};
    }
    else {
        const matchesOf2016 = matches.filter((match) => {
            return match.season === '2016';
        });
        let idOf2016 = matchesOf2016.map(value => value.id);
        const extraRunsPerTeam = deliveries.reduce((extraRunsPerTeam, delivery) => {
            if (idOf2016.includes(delivery.match_id)) {
                if (extraRunsPerTeam[delivery.bowling_team] === undefined) {
                    extraRunsPerTeam[delivery.bowling_team] = Number(delivery.extra_runs);
                }
                else {
                    extraRunsPerTeam[delivery.bowling_team] += Number(delivery.extra_runs);
                }
            }
            return extraRunsPerTeam;
        }, {});
        return extraRunsPerTeam;
    }
}
module.exports = extraRunsConcededPerTeamIn2016;