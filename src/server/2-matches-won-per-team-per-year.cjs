function matchesWonPerTeamPerYear(matches) {
    if (matches === undefined || !Array.isArray(matches)) {
        return {};
    }
    else {
        const teamWonPerYear = matches.reduce((teamWonPerYear, match) => {
            if (match.winner in teamWonPerYear) {
                if (match.season in teamWonPerYear[match.winner]) {
                    teamWonPerYear[match.winner][match.season]++;
                }
                else {
                    teamWonPerYear[match.winner][match.season] = 1
                }
            }
            else {
                teamWonPerYear[match.winner] = {}
                teamWonPerYear[match.winner][match.season] = 1;
            }
            return teamWonPerYear;
        }, {});
        return teamWonPerYear;
    }
}

module.exports = matchesWonPerTeamPerYear;