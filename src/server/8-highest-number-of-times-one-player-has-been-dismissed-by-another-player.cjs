function highestNumberOfTimesOnePlayerDismissedByAnotherPlayer(deliveries) {
    if (deliveries === undefined) {
        return {};
    }
    else {
        const playerData = deliveries.reduce((batsmanDismissed, delivery) => {
            if (delivery.dismissal_kind !== "" && delivery.dismissal_kind !== "delivery out") {

                if (batsmanDismissed[delivery.batsman]) {
                    if (batsmanDismissed[delivery.batsman][delivery.bowler]) {
                        batsmanDismissed[delivery.batsman][delivery.bowler] += 1
                    }
                    else {
                        batsmanDismissed[delivery.batsman][delivery.bowler] = 1;
                    }
                }
                else {
                    batsmanDismissed[delivery.batsman] = {};
                    if (batsmanDismissed[delivery.batsman][delivery.bowler]) {
                        batsmanDismissed[delivery.batsman][delivery.bowler] = +1;
                    }
                    else {
                        batsmanDismissed[delivery.batsman][delivery.bowler] = 1;
                    }
                }
            }

            return batsmanDismissed;
        }, {});

        let dismissedPlayer = {};
        let minimum = Number.MIN_VALUE;
        Object.entries(playerData).map((batsman) => {
            const dismissedBowler = Object.entries(batsman[1])
                .sort((previousBatman, currentBatman) => currentBatman[1] - previousBatman[1])
                .slice(0, 1);
            if (dismissedBowler[0][1] > minimum) {
                minimum = dismissedBowler[0][1];
                let dataObj = {};
                dataObj[batsman[0]] = dismissedBowler[0];
                dismissedPlayer = dataObj;
            }
        });

        return dismissedPlayer;
    }

}

module.exports = highestNumberOfTimesOnePlayerDismissedByAnotherPlayer;