function tossWinMatchWin(matches) {
    if (matches === undefined || !Array.isArray(matches)) {
        return {};
    }
    else {
        const teamWonMatchAndToss = matches.reduce((teamWonMatchAndToss, match) => {
            if (teamWonMatchAndToss[match.winner] && match.toss_winner === match.winner) {
                teamWonMatchAndToss[match.winner]++;
            }
            else if (match.toss_winner === match.winner) {
                teamWonMatchAndToss[match.winner] = 1;
            }
            return teamWonMatchAndToss;
        }, {});
        return teamWonMatchAndToss;
    }
}

module.exports = tossWinMatchWin;