function bestEconomyBowlerSuperover(deliveries) {
    if (deliveries === undefined) {
        return {};
    }
    else {
        const bowlerEconomyRun = deliveries.filter((ball) => {
            return ball.is_super_over !== "0";
        }).reduce((superOverBowler, delivery) => {
            if (superOverBowler[delivery.bowler]) {
                superOverBowler[delivery.bowler].runs += Number(delivery.total_runs);
                superOverBowler[delivery.bowler].ball += 1;
                superOverBowler[delivery.bowler].economyValue = (
                    superOverBowler[delivery.bowler].runs /
                    (superOverBowler[delivery.bowler].ball / 6)
                ).toFixed(2);
            } else {
                superOverBowler[delivery.bowler] = {};
                superOverBowler[delivery.bowler].runs = Number(delivery.total_runs);
                superOverBowler[delivery.bowler].ball = 1;
            }

            return superOverBowler;
        }, {});
        let bowlerEconomy = Object.entries(bowlerEconomyRun).reduce(
            (bowlerName, bowlerEconomyValue) => {
                bowlerName[bowlerEconomyValue[0]] = Number(bowlerEconomyValue[1].economyValue);
                return bowlerName;
            }, {});
        bowlerEconomy = Object.entries(bowlerEconomy).sort((bowler1, bowler2) => {
            return bowler1[1] - bowler2[1];
        })
        const bestEconomyBowler = Object.fromEntries(bowlerEconomy.slice(0, 1));

        return bestEconomyBowler;
    }
}
module.exports = bestEconomyBowlerSuperover;