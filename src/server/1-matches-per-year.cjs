
function matchesPerYear(matches) {

    if (matches === undefined || !Array.isArray(matches)) {
        return {};
    }
    else {
        const matchesPlayed = matches.reduce((matchesPlayed, match) => {
            if (matchesPlayed[match.season]) {
                matchesPlayed[match.season]++;
            }
            else {
                matchesPlayed[match.season] = 1;
            }
            return matchesPlayed;
        }, {});
        return matchesPlayed;
    }
}

module.exports = matchesPerYear;