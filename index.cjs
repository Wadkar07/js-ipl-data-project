const csv = require('csv-parser')
const fs = require('fs')
const path = require('path');

const MATCHES_DATA = './src/data/matches.csv'
const DELIVERIES_DATA = './src/data/deliveries.csv'
const OUTPUT_DIRECTORY = './src/public/output';

const matchesPerYear = require('./src/server/1-matches-per-year.cjs');
const matchesWonPerTeamPerYear = require('./src/server/2-matches-won-per-team-per-year.cjs');
const extraRunsConcededPerTeamIn2016 = require('./src/server/3-extra-runs-conceded-per-team-in-2016.cjs');
const top10EconomicalBowlerIn2015 = require('./src/server/4-top-10-economical-bowlers-in-2015.cjs');
const tossWinMatchWin = require('./src/server/5-won-toss-and-match-by-each-team.cjs');
const playerOfTheMatchForEachSeason = require('./src/server/6-player-of-the-match-for-each-season.cjs');
const strikeRateOfBatsmanForEachSeason = require('./src/server/7-strike-rate-of-batsman-for-each-season.cjs')
const highestNumberOfTimesOnePlayerDismissedByAnotherPlayer =require('./src/server/8-highest-number-of-times-one-player-has-been-dismissed-by-another-player.cjs')
const bestEconomyBowlerSuperover = require('./src/server/9-best-economy-bowler-superover.cjs');

const matches = [];
const deliveries = [];
fs.createReadStream(MATCHES_DATA)
  .pipe(csv())
  .on('data', (data) => matches.push(data))
  .on('end', () => {
    fs.createReadStream(DELIVERIES_DATA)
      .pipe(csv())
      .on('data', (data) => deliveries.push(data))
      .on('end', () => {
        let totalMatchesPlayedPerYear = matchesPerYear(matches);
        try {
          totalMatchesPlayedPerYear = JSON.stringify(totalMatchesPlayedPerYear, null, " ");
        }
        catch (error) {
          console.error(error);
        }

        fs.writeFile(path.join(OUTPUT_DIRECTORY, '1-matches-per-year.json'), totalMatchesPlayedPerYear, (error) => {
          if (error) {
            console.error(error);
          }
        });


        let totalmatchesWonPerTeamPerYear = matchesWonPerTeamPerYear(matches);
        try {
          totalmatchesWonPerTeamPerYear = JSON.stringify(totalmatchesWonPerTeamPerYear, null, " ");
        }
        catch (error) {
          console.error(error);
        }

        fs.writeFile(path.join(OUTPUT_DIRECTORY, '2-matches-won-per-team-per-year.json'), totalmatchesWonPerTeamPerYear, (error) => {
          if (error) {
            console.error(error);
          }
        });

        let totalextraRunsConcededPerTeamIn2016 = extraRunsConcededPerTeamIn2016(matches, deliveries);
        try {
          totalextraRunsConcededPerTeamIn2016 = JSON.stringify(totalextraRunsConcededPerTeamIn2016, null, " ");
        }
        catch (error) {
          console.log(error);
        }
        fs.writeFile(path.join(OUTPUT_DIRECTORY, '3-extra-runs-conceded-per-team-in-2016.json'), totalextraRunsConcededPerTeamIn2016, (error) => {
          if (error) {
            console.error(error);
          }
        });

        
        let totaltop10EconomicalBowlerIn2015 = top10EconomicalBowlerIn2015(matches,deliveries);
        try {
          totaltop10EconomicalBowlerIn2015 = JSON.stringify(totaltop10EconomicalBowlerIn2015, null, 4);
        }
        catch (error) {
          console.log(error);
        }
        fs.writeFile(path.join(OUTPUT_DIRECTORY, '4-top-10-economical-bowlers-in-2015.json'), totaltop10EconomicalBowlerIn2015, (error) => {
          if (error) {
            console.error(error);
          }
        });

        let totaltossWinMatchWin = tossWinMatchWin(matches);
        try {
          totaltossWinMatchWin = JSON.stringify(totaltossWinMatchWin, null, 4);
        }
        catch (error) {
          console.log(error);
        }
        fs.writeFile(path.join(OUTPUT_DIRECTORY, '5-team-won-toss-and-match.json'), totaltossWinMatchWin, (error) => {
          if (error) {
            console.error(error);
          }
        });
        
        let totalPlayerOfTheMatchForEachSeason = playerOfTheMatchForEachSeason(matches);
        try {
          totalPlayerOfTheMatchForEachSeason = JSON.stringify(totalPlayerOfTheMatchForEachSeason, null, 4);
        }
        catch (error) {
          console.log(error);
        }
        fs.writeFile(path.join(OUTPUT_DIRECTORY, '6-player-of-the-match-for-each-season.json'), totalPlayerOfTheMatchForEachSeason, (error) => {
          if (error) {
            console.error(error);
          }
        });
        
        let totalStrikeRateOfBatsmanForEachSeason = strikeRateOfBatsmanForEachSeason(matches,deliveries);
        try{
          totalStrikeRateOfBatsmanForEachSeason = JSON.stringify(totalStrikeRateOfBatsmanForEachSeason,null,5);
        }
        catch(error){
          console.log(error);
        }
        fs.writeFile(path.join(OUTPUT_DIRECTORY,'7-strike-rate-of-batsman-for-each-season.json'),totalStrikeRateOfBatsmanForEachSeason, (error)=>{
          if(error){
            console.log(error);
          }
        });


        
        let totalhighestNumberOfTimesOnePlayerDismissedByAnotherPlayer = highestNumberOfTimesOnePlayerDismissedByAnotherPlayer(deliveries);
        try{
          totalhighestNumberOfTimesOnePlayerDismissedByAnotherPlayer = JSON.stringify(totalhighestNumberOfTimesOnePlayerDismissedByAnotherPlayer,null,5);
        }
        catch(error){
          console.log(error);
        }
        fs.writeFile(path.join(OUTPUT_DIRECTORY,'8-highest-number-of-times-one-player-has-been-dismissed-by-another-player.json'),totalhighestNumberOfTimesOnePlayerDismissedByAnotherPlayer, (error)=>{
          if(error){
            console.log(error);
          }
        });

        let totalbestEconomyBowlerSuperover = bestEconomyBowlerSuperover(deliveries);
        try{
          totalbestEconomyBowlerSuperover = JSON.stringify(totalbestEconomyBowlerSuperover,null,5);
        }
        catch(error){
          console.log(error);
        }
        fs.writeFile(path.join(OUTPUT_DIRECTORY,'9-best-economy-bowler-superover.json'),totalbestEconomyBowlerSuperover, (error)=>{
          if(error){
            console.log(error);
          }
        });
      });
  });
